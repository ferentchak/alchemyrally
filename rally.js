var rallyToolkit = require('rally'),
    queryUtils = rallyToolkit.util.query,
    Q = require('q'),
    _ = require('lodash');

function initRally(user,pass,server){
  return rallyToolkit({
    user:user,
    pass:pass,
    server:server
  });
}

function Rally(user,pass,server){
  this.restApi = initRally(user,pass,server);
}

Rally.prototype.getPortfolioItems = function(){
  var promise = this.restApi.query({
    type: 'portfolioitem/feature', 
    limit: 300,
    fetch: ['Name', 'Description','CreationDate','Project','ObjectID'],
    order: 'CreationDate desc'
  })
  .then(function(response){
    var results = _.map(response.Results,function(portfolioItem){
      return {
        Name:portfolioItem.Name,
        ObjectID: portfolioItem.ObjectID,
        Project: portfolioItem.Project._refObjectName,
        CreationDate: portfolioItem.CreationDate,
        Description: portfolioItem.Description
      };
    });
    console.log(results.length + ' portfolio items to process');
    return Q(results);
  });
  return promise;
};
module.exports = Rally;
