var chai = require('chai'),
    expect = chai.expect,
    throttle = require('../throttle'),
    Q = require('q'),
    _ = require('lodash');

describe('throttle', function() {
  it('should allow only five function calls to run at a time on a large array of values', function(done) {
    var currentCount = 0;
    throttle(
      ['a','b','c','d','e','f','g','h','i','j'],
      5,
      function(v){
        currentCount++;
        return Q(v).delay(2).then(function(v){
          currentCount--;
          if (currentCount > 5)
            return Q.reject('Too many simultaneously executed functions'); 
          return Q(v);
        });
      }
      )
    .then(done,done);
  });

  it('should notify for each call', function(done) {
    var uncalled = {
      a:1,
      b:1,
      c:1
    };
    throttle(
      ['a','b','c'],
      5,
      function(v){return Q(v);}
    )
    .then(
      function(){
        expect(_.keys(uncalled), 'uncalled notify').with.length(0);
      },
      null,
      function(v){
        delete uncalled[v];
      }
    ).
    then(done,done);

  });

});
