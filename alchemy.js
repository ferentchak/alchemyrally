var Q = require('q'),
  AlchemyAPI = require('./AlchemyToolkit');

function Alchemy(apiKey){
  var alchemyapi = new AlchemyAPI(apiKey);
  this.GetPISentiment = function(portfolioItem){
    var deferred = Q.defer();
    var htmlData = '<h1>' + portfolioItem.Name + '</h1>' + portfolioItem.Description;
    alchemyapi.keywords(
      'html', 
      htmlData, 
      { 'sentiment':1 }, 
      function(response) {      
        deferred.resolve({
          input:portfolioItem,
          response:response.keywords
        });
      }
    );
    return deferred.promise;
  };
}

module.exports = Alchemy;
