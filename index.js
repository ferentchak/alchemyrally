var fs = require('fs'),
  check = require('promise-to-validate').check,
  value = require('promise-to-validate').value,
  rally = require('./rally'),
  Q = require('q'),
  throttle = require('./throttle'),
  store = require('./store'),
  Alchemy = require('./alchemy');

var credentialsFile = './credentials.json';

if(!fs.existsSync(credentialsFile)){
  console.error('Credentials.json is required. That file was now created please fill it with your credentials.');
  var defaultCredentials = {
    'rallyServer':'https://rally1.rallydev.com',
    'rallyPassword':'',
    'rallyUsername':'',
    'alchemyApiKey':''
  };
  fs.writeFileSync(credentialsFile,JSON.stringify(defaultCredentials, null, 2));
  process.exit(1);
}
var credentials = require(credentialsFile);
check(credentials)
.where(
  value('rallyServer').isUrl(),
  value('rallyPassword').len(2,200),
  value('rallyUsername').len(2,200),
  value('alchemyApiKey').len(40,40)
)
.then(function(errors){
  if(errors){
    console.error(errors);
    return Q.reject('Error in credentials.json');
  }
  var r = new rally(credentials.rallyUsername,credentials.rallyPassword,credentials.rallyServer);
  return r.getPortfolioItems();
})
.then(function(portfolioItems){
  var alchemyapi = new Alchemy(credentials.alchemyApiKey);	  
  var count = 0;
  var savedCount = 0;
  return throttle(portfolioItems,5,alchemyapi.GetPISentiment)
    .then(
      function(){
        console.log("PorfoloItems Saved" + savedCount);
      }
      ,null,
      function(response){
        console.log(count++ + ' portfolio items processed');
        store(response)
        .then(function(){
          savedCount++;
        });
      }
    );
})
.fail(function (error){
  console.error('Error',error);
  console.error(error.stack);
  process.exit(1);
});
module.exports = {};
