var Q = require('q'),
  mongoskin = require('mongoskin'),
  mongoUri = process.env.MONGOLAB_URI,
  db = mongoskin.db(mongoUri, {safe:true}),
  collection = db.collection('AlchemyPortfolioItems');

function store(input){
    var deferred = Q.defer();
    collection.insert(input, {}, function(e, results){
      if(e){
        deferred.reject(e);
      }
      else{
        deferred.resolve(results._id+'');
      }
    });
    return deferred.promise;
  }


module.exports = store;

