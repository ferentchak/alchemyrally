# Alchemy to Rally 

Extracts all Rally features and processes it through the AlchemyAPI keywords extraction process.

The resulting data is sent into MongoLabs for later analysis.
